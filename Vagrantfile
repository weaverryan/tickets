# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = 'casualjim/trusty-vagrant'

  config.ssh.insert_key = false

  config.vm.define 'knp-db-01-dev' do |db|
    db.vm.hostname = 'knp-db-01-dev'
    db.vm.network "forwarded_port", guest: 3306, host: 3336
    db.vm.network "private_network", ip: "192.168.33.51"
  end

  config.vm.define 'knp-web-01-dev', primary: true do |web|
    web.vm.hostname = 'knp-web-01-dev'
    web.vm.network "forwarded_port", guest: 80, host: 8080
    web.vm.network "private_network", ip: "192.168.33.50"
    web.vm.synced_folder ".", "/srv/www/tickets/web", :nfs => true

    # Provisioning workaround to prevent playbook replays
    # Possible solution: groups
    web.vm.provision "ansible" do |ansible|
        ansible.inventory_path = "provisioning/ansible/development"
        ansible.playbook = "provisioning/ansible/site.yml"
        ansible.limit = 'all'
        ansible.sudo = true
        ansible.verbose = 'vvvv'
      end
  end

  config.vm.provider "vmware_fusion" do |v|
    v.vmx["memsize"] = "2048"
  end
end
