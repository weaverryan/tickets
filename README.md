Tickets!
========

A simple ticketing application based on the "Symfony Demo Application".

Requirements
------------

  * PHP 5.4 or higher;
  * and the [usual Symfony application requirements](http://symfony.com/doc/current/reference/requirements.html).

Usage
-----

Just use the built-in web server:

```bash
$ cd tickets
$ php app/console server:run
```

This command will start a web server for the Symfony application. Now you can access
the application in your browser at <http://localhost:8000>. You can stop the built-in
web server by pressing `Ctrl + C` while you're in the terminal.
