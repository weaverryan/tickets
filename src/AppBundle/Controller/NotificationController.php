<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Ticket;
use AppBundle\Form\EditTicketType;
use AppBundle\Form\TicketType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NotificationController extends Controller
{
    /**
     * @Route("/notifications", name="notification_list")
     */
    public function listAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        $notifications = $this->getDoctrine()
            ->getRepository('AppBundle:UserNotification')
            ->findBy(
                ['user' => $this->getUser()],
                ['createdAt' => 'DESC']
            );

        return $this->render('notification/list.html.twig', [
            'notifications' => $notifications
        ]);
    }
}