<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Ticket;
use AppBundle\Form\EditTicketType;
use AppBundle\Form\TicketType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TicketController extends Controller
{
    /**
     * @Route("/tickets/new", name="ticket_new")
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        $ticket = new Ticket();
        $form = $this->createForm(new TicketType(), $ticket);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // the ticket is created by this user
            $user = $this->getUser();
            $ticket->setCreatedByUser($user);

            // initialize the status to active
            $activeStatus = $em->getRepository('AppBundle:TicketStatus')
                ->findOneBy(['machineName' => 'active']);
            $ticket->setStatus($activeStatus);

            $assignTo = $em->getRepository('AppBundle:User')
                ->findOneBy(['isOnSupportTeam' => true]);
            $ticket->setAssignedToUser($assignTo);

            $em->persist($ticket);
            $em->flush();

            $this->addFlash('success', 'Ticket created!');

            $url = $this->generateUrl('ticket_list');
            return $this->redirect($url);
        }

        return $this->render('ticket/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/tickets", name="ticket_list")
     */
    public function listAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $em = $this->getDoctrine()->getManager();

        $myTickets = $em->getRepository('AppBundle:Ticket')
            ->findBy(
                ['createdByUser' => $this->getUser()],
                ['createdAt' => 'DESC']
            );

        $assignedToMeTickets = $em->getRepository('AppBundle:Ticket')
            ->findBy(
                ['assignedToUser' => $this->getUser()],
                ['createdAt' => 'DESC']
            );

        return $this->render('ticket/list.html.twig', [
            'myTickets' => $myTickets,
            'assignedToMeTickets' => $assignedToMeTickets,
        ]);
    }

    /**
     * @Route("/tickets/{id}", name="ticket_show")
     */
    public function showAction(Ticket $ticket, Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        $form = $this->createForm(new EditTicketType(), $ticket);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($ticket);
            $em->flush();

            $this->addFlash('success', 'Ticket updated!');

            $url = $this->generateUrl('ticket_show', [
                'id' => $ticket->getId()
            ]);
            return $this->redirect($url);
        }

        return $this->render('ticket/show.html.twig', [
            'form' => $form->createView(),
            'ticket' => $ticket
        ]);
    }
}