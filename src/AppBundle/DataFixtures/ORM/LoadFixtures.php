<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Ticket;
use AppBundle\Entity\TicketStatus;
use AppBundle\Entity\User;
use AppBundle\Entity\Post;
use AppBundle\Entity\Comment;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the sample data to load in the database when executing this command:
 *   $ php app/console doctrine:fixtures:load
 *
 * See http://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class LoadFixtures implements FixtureInterface, ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    private $supportTeamUsers = array();

    private $normalUsers = array();

    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        $this->loadTickets($manager);
    }

    private function loadUsers(ObjectManager $manager)
    {
        $passwordEncoder = $this->container->get('security.password_encoder');

        $johnUser = new User();
        $johnUser->setUsername('john_user');
        $johnUser->setEmail('john_user@symfony.com');
        $johnUser->setIsOnSupportTeam(false);
        $encodedPassword = $passwordEncoder->encodePassword($johnUser, 'kitten');
        $johnUser->setPassword($encodedPassword);
        $manager->persist($johnUser);
        $this->normalUsers[] = $johnUser;

        $ryanUser = new User();
        $ryanUser->setUsername('ryan_user');
        $ryanUser->setEmail('ryan_user@symfony.com');
        $ryanUser->setIsOnSupportTeam(false);
        $encodedPassword = $passwordEncoder->encodePassword($ryanUser, 'kitten');
        $ryanUser->setPassword($encodedPassword);
        $manager->persist($ryanUser);
        $this->normalUsers[] = $ryanUser;

        $annaAdmin = new User();
        $annaAdmin->setUsername('anna_admin');
        $annaAdmin->setEmail('anna_admin@symfony.com');
        $annaAdmin->setRoles(array('ROLE_ADMIN'));
        $annaAdmin->setIsOnSupportTeam(true);
        $encodedPassword = $passwordEncoder->encodePassword($annaAdmin, 'kitten');
        $annaAdmin->setPassword($encodedPassword);
        $manager->persist($annaAdmin);
        $this->supportTeamUsers[] = $annaAdmin;

        $leannaAdmin = new User();
        $leannaAdmin->setUsername('leanna_admin');
        $leannaAdmin->setEmail('leanna_admin@symfony.com');
        $leannaAdmin->setRoles(array('ROLE_ADMIN'));
        $leannaAdmin->setIsOnSupportTeam(true);
        $encodedPassword = $passwordEncoder->encodePassword($leannaAdmin, 'kitten');
        $leannaAdmin->setPassword($encodedPassword);
        $manager->persist($leannaAdmin);
        $this->supportTeamUsers[] = $leannaAdmin;

        $manager->flush();
    }

    private function loadTickets(ObjectManager $manager)
    {
        $statusActive = $this->createTicketStatus('active', 'Open');
        $statusClosed = $this->createTicketStatus('closed', 'Closed');
        $statusOnHold = $this->createTicketStatus('on_hold', 'On Hold');
        $statuses = [$statusActive, $statusClosed, $statusOnHold];

        $priorities = ['low', 'medium', 'high'];
        for ($i = 0; $i < 25; $i++) {
            $ticket = new Ticket();
            $ticket->setTitle('Ticket Item '.$i);
            $ticket->setDescription('Lorem ipsum dolor');

            $priorityKey = array_rand($priorities);
            $ticket->setPriority($priorities[$priorityKey]);

            $statusKey = array_rand($statuses);
            $ticket->setStatus($statuses[$statusKey]);

            $createdUserKey = array_rand($this->normalUsers);
            $ticket->setCreatedByUser($this->normalUsers[$createdUserKey]);

            $assignedToUserKey = array_rand($this->supportTeamUsers);
            $ticket->setAssignedToUser($this->supportTeamUsers[$assignedToUserKey]);

            $ticket->setCreatedAt(new \DateTime('-'.$i.' minutes'));

            $manager->persist($ticket);
        }

        $manager->flush();
    }

    private function createTicketStatus($key, $name)
    {
        $status = new TicketStatus();
        $status->setMachineName($key);
        $status->setName($name);

        $this->getEntityManager()->persist($status);

        return $status;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    private function getEntityManager()
    {
        return $this->container->get('doctrine.orm.default_entity_manager');
    }
}
