<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditTicketType extends TicketType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('status', 'entity', [
            'class' => 'AppBundle\Entity\TicketStatus',
            'property' => 'name'
        ]);
        // make a fake disabled field, just to show it
        $builder->add('createdByEmail', 'text', [
            'disabled' => true,
            'property_path' => 'createdByUser.email',
            'label' => 'Created By',
        ]);
        // make a fake disabled field, just to show it
        $builder->add('assignedToEmail', 'text', [
            'disabled' => true,
            'property_path' => 'assignedToUser.email',
            'label' => 'Assigned To',
        ]);
    }

    public function getName()
    {
        return 'edit_ticket';
    }
}
